package stratified.medical.service.tennis.api;

import static org.junit.Assert.assertEquals;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:tennisTestContext.xml"})
public class TennisServiceTest  {

    @Autowired
    private TennisService tennisService;
    
	private String playerA;
	private String playerB;
	
	private final static String playerC = "this player is not playing";
	
	private final static int MIN_POINTS_TO_WIN = 4 * 6 * 2;
	
    /**
     * Prepare the unit tests
     */
    @Before
    public void prepare() {
    	playerA = UUID.randomUUID().toString();
    	playerB = UUID.randomUUID().toString();
    }
    
	@Test
    public void shouldBeAbleToCreateMatch()  {
		tennisService.createMatch(playerA, playerB);
    }

    @Test(expected = RuntimeException.class)
    public void shouldNotBeAbleToCreateMatchWithPlayerWhoIsAlreadyPlayingAGame()  {
		tennisService.createMatch(playerA, playerB);
		tennisService.createMatch(playerA, playerC);
    }

    @Test(expected = RuntimeException.class)
    public void shouldNotBeAbleToCreateMatchWithTheSamePlayer()  {
		tennisService.createMatch(playerA, playerA);
    }
    
    @Test(expected = RuntimeException.class)
    public void shouldNotBeAbleToUpdateScoreIfGameNotStarted()  {
		tennisService.updateScore(playerA); 
    }
    
    @Test(expected = RuntimeException.class)
    public void shouldNotBeAbleToUpdateScoreIfNotPartOfTheGame()  {
    	
		tennisService.createMatch(playerA, playerA);
		tennisService.updateScore(playerC); // not playing!
    }
    
	@Test
    public void shouldBeAbleToUpdateScoreForPlayerA()  {
		tennisService.createMatch(playerA, playerB);
		tennisService.updateScore(playerA);
    }
	
	@Test
    public void shouldBeAbleToUpdateScoreForPlayerB()  {
		tennisService.createMatch(playerA, playerB);
		tennisService.updateScore(playerB);
    }

	@Test
    public void shouldWinTheMatchWith48points()  {
		
		String id = tennisService.createMatch(playerA, playerB);
		
		for(int i=0;i<MIN_POINTS_TO_WIN;i++) { // super good player
			tennisService.updateScore(playerB); // scored 48 points in a row
		}
		
		// and won the match
		assertEquals(playerB, tennisService.getStatus(id).getWinner().getName());
    }

	@Test
    public void shouldHaveOsetsWonAfterWinningJust3Points()  {
		
    	String id =  tennisService.createMatch(playerA, playerB);

    	int pointsWon = 3;
		for(int i=0;i<pointsWon;i++) {
			tennisService.updateScore(playerB);
		}
		assertEquals(tennisService.getStatus(id).getPlayerB().getScore().getSets(), 0);
    }
	

	@Test
    public void shouldHaveOgamesWonAfterWinningJust3Points()  {
		
    	String id =  tennisService.createMatch(playerA, playerB);

    	int pointsWon = 3;
		for(int i=0;i<pointsWon;i++) {
			tennisService.updateScore(playerB);
		}
		assertEquals(tennisService.getStatus(id).getPlayerB().getScore().getGames(), 0);
    }

	@Test
    public void shouldHave3gamesWonAfterWinning12Points()  {
		
    	String id =  tennisService.createMatch(playerA, playerB);
		
    	int pointsWon = 4*3;
		for(int i=0;i<pointsWon;i++) {
			tennisService.updateScore(playerB);
		}
		assertEquals(tennisService.getStatus(id).getPlayerB().getScore().getGames(), 3);
    }

	@Test
    public void shouldHave1gamesWonAfterWinning4Points()  {
		
    	String id =  tennisService.createMatch(playerA, playerB);
		
    	int pointsWon = 4;
		for(int i=0;i<pointsWon;i++) {
			tennisService.updateScore(playerB);
		}
		assertEquals(tennisService.getStatus(id).getPlayerB().getScore().getGames(), 1);
    }

    @Test(expected = RuntimeException.class)
    public void shouldNotBeAbleToUpdateScoreTooManyTimes() {
		
		tennisService.createMatch(playerA, playerB);
		
		int illegalNumberOfPoints = 10000; // match should be over by then
		for(int i=0;i<illegalNumberOfPoints;i++) {
			tennisService.updateScore(playerB);
		}
    }
}
