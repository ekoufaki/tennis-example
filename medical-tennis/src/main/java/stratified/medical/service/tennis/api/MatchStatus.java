package stratified.medical.service.tennis.api;

import java.util.UUID;

/**
 * Status for a Tennis Match.
 */
public class MatchStatus {
	
	/* In this tennis universe, winning 2 sets makes you the winner */
	private final static int SETS_TO_WIN_A_MATCH = 2;

	private String id;
	
	private Player playerA;
	private Player playerB;
	
	private Player winner;

	private long duration;
	
	private boolean hasFinished;
	
	public MatchStatus(String playerA, String playerB) {
		
		id = UUID.randomUUID().toString();
		
		this.playerA = new Player(playerA);
		this.playerB = new Player(playerB);
		
		winner = null;
	}
	
	public String getId() {
		return id;
	}
	
	public Player getPlayerA() {
		return playerA;
	}
	
	public Player getPlayerB() {
		return playerB;
	}
	
	public Player getPlayer(String player) {
		
		if (player.equals(playerA.getName())) {
			return playerA;
		}
		if (player.equals(playerB.getName())) {
			return playerB;
		}
		return null;
	}
	
	public boolean isPlaying(String player) {
		return playerA.getName().equals(player) || playerB.getName().equals(player);
	}
	
	public long getDuration() {
		return duration;
	}
	
	public boolean hasFinished() {
		return hasFinished;
	}
	
	public void updateScore(String player) {
		
		if (hasFinished()) {
			throw new RuntimeException("Cannot update game that has finished!");
		}
		getPlayer(player).updateScore();
		checkIfGameFinished();
	}
	
	private void checkIfGameFinished() {
		
		if (playerA.getScore().getSets() == SETS_TO_WIN_A_MATCH) {
			hasFinished = true;
			winner = playerA;
		}
		else if (playerB.getScore().getSets() == SETS_TO_WIN_A_MATCH) {
			hasFinished = true;
			winner = playerB;
		}
	}
	
	public Player getWinner() {
		return winner;
	}
}
