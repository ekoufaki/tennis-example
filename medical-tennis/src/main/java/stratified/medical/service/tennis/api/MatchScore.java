package stratified.medical.service.tennis.api;

/**
 * Represents the score of ONE PLAYER for a Tennis Match.
 * 
 * Points: "0", "15", "30", "40"
 * Games: 1-6
 * Sets: 1-3
 */
public class MatchScore {

	private int games;
	private String points;
	private int sets;
	
	private final static int GAMES_TO_WIN_SET = 6;

	public MatchScore() {
		
		games = 0;
		points = "00";
		sets = 0;
	}

	public int getGames() {
		return games;
	}

	public String getPoints() {
		return points;
	}

	public int getSets() {
		return sets;
	}

	public void setGames(int games) {
		this.games = games;
	}

	public void setPoints(String points) {
		this.points = points;
	}

	public void setSets(int sets) {
		this.sets = sets;
	}

	public void updateScore() {

		if (points.equals("00")) {
			points = "15";
		} else if (points.equals("15")) {
			points = ("30");
		} else if (points.equals("30")) {
			points = ("40");
		} else if (points.equals("40")) {
			gameWon();
		}
	}
	
	public boolean hasWonSet() {
		
		if (games == GAMES_TO_WIN_SET) {
			return true;
		}
		return false;
	}
	
	public void resetPoints() {
		points = "00";
	}
	
	public void setWon() {
		sets++;
		games = 0;
	}
	
	private void gameWon() {
		resetPoints();
		games += 1;
		if (hasWonSet()) {
			setWon();
		}
	}
}
