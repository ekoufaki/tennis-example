package stratified.medical.service.tennis.api;

/**
 * Represents a Player for a Tennis Match.
 */
public class Player {
	
	private MatchScore score;
	private String name;
	
	public Player(String name) {
		this.name = name;
		this.score = new MatchScore();
	}
	
	public String getName() {
		return name;
	}
	
	public MatchScore getScore() {
		return score;
	}
	
	public void updateScore() {
		score.updateScore();
	}
}
