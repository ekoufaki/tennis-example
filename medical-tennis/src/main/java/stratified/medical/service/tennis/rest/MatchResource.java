package stratified.medical.service.tennis.rest;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import stratified.medical.service.tennis.api.MatchStatus;
import stratified.medical.service.tennis.api.TennisService;

/**
 * Resource to create / update / fetch Tennis Matches
 */
@Path("/matches/{matchId}")
@Component
public class MatchResource {

    @Autowired
    private TennisService tennisService;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public MatchStatus getMatch(@PathParam("matchId") String matchId) {

        return tennisService.getStatus(matchId);
    }
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createMatch(String playerA, String playerB) {

        if (playerA != null && playerB != null) {
            String id = tennisService.createMatch(playerA, playerB);
            try {
				return Response.created(new URI(id)).build();
			} catch (URISyntaxException e) {
			}
        }
        return Response.notModified().build();
    }

    @PUT
    @Path("/{player}")
    @Consumes({MediaType.APPLICATION_JSON})
    public void updateMatch(@PathParam("player") String player) {
        if (player != null) {
            // TODO: modify updateScore() to use {matchId}
            tennisService.updateScore(player);
        }
    }
}
