package stratified.medical.service.tennis.api;


public interface TennisService {
	
    /**
     * Starts a new match with the given players.
     * 
     * @return Unique identifier representing this match
     */
    String createMatch(String playerA, String playerB);

    /**
     * Updates the score
     * 
     * @param player who won the last point
     */
    void updateScore(String player);
    
    /**
     * @return Current match status (score, players, duration)
     * 
     * @param matchId Unique identifier representing this match
     */
    MatchStatus getStatus(String matchId);
}
