package stratified.medical.service.tennis.inmemory;

import java.util.HashMap;
import java.util.Iterator;

import stratified.medical.service.tennis.api.MatchStatus;
import stratified.medical.service.tennis.api.TennisService;

/**
 * InMemory implementation of the Tennis Service using {@link HashMap}.
 */
public class InMemoryTennisService implements TennisService {
	
	private HashMap<String, MatchStatus> matches;
	
	public InMemoryTennisService() {
		 matches = new HashMap<String, MatchStatus>();
	}

	public String createMatch(String playerA, String playerB) {
		
		if (playerA.equals(playerB)) {
			throw new RuntimeException("Players dont like to play against themselves!");
		}
		
		if (getActiveMatch(playerA) != null || getActiveMatch(playerB) != null) {
			throw new RuntimeException("Player is already playing in a match!");
		}
		
		MatchStatus m = new MatchStatus(playerA, playerB);
		
		String id = m.getId();
		matches.put(id, m);
		
		return id;
	}
	
	/**
	 * @return {@link MatchStatus} where the specified player is currently playing,
	 * or null if no active match is found.
	 */
	private MatchStatus getActiveMatch(String player) {
		
		Iterator<MatchStatus> i = matches.values().iterator();
		while(i.hasNext()){
			MatchStatus currentMatch = i.next(); 
			if (!currentMatch.hasFinished() && currentMatch.isPlaying(player))
				return currentMatch;
		}
		return null;
	}

	public void updateScore(String player) {
		
		MatchStatus m = getActiveMatch(player);
		if (m == null) {
			throw new RuntimeException("It seems this player is not currently playing!");
		}
		m.updateScore(player);
		matches.put(m.getId(), m);
	}
	
	public MatchStatus getStatus(String matchId) {
		return matches.get(matchId);
	}
}
